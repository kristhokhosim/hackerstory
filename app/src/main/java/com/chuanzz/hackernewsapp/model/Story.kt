package com.chuanzz.hackernewsapp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Story(
    @field:SerializedName("id")
    val id: Long,
    @field:SerializedName("score")
    val score: Int? = null,
    @field:SerializedName("title")
    val title: String? = null,
    @field:SerializedName("by")
    val by: String? = null,
    @field:SerializedName("kids")
    val kids: List<Int>?
) : Parcelable