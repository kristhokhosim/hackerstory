package com.chuanzz.hackernewsapp.model

import com.google.gson.annotations.SerializedName

data class Comment(
    @field:SerializedName("id")
    val id: Long?,
    @field:SerializedName("by")
    val by: String?,
    @field:SerializedName("kids")
    val kids: String?,
    @field:SerializedName("parent")
    val parent: Long?,
    @field:SerializedName("text")
    val komentar: String?,
    @field:SerializedName("time")
    val time: Long?
)