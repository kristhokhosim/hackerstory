package com.chuanzz.hackernewsapp.base

import androidx.annotation.StringRes

interface MvpView {
    fun showLoading()

    fun hideLoading()

    fun onError(message:String)
}