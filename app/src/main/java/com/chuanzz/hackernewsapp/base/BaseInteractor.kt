package com.chuanzz.hackernewsapp.base

import com.chuanzz.hackernewsapp.api.ApiHelper

open class BaseInteractor(apiHelper: ApiHelper) : MvpInteractor {

    private val mApiHelper: ApiHelper? = apiHelper

    override fun getApiHelper(): ApiHelper? {
        return mApiHelper
    }
}