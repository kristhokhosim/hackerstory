package com.chuanzz.hackernewsapp.base

import android.os.Bundle
import android.transition.Fade
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.androidnetworking.AndroidNetworking
import com.chuanzz.hackernewsapp.AppUtils
import com.chuanzz.hackernewsapp.R
import com.chuanzz.hackernewsapp.di.component.ActivityComponent
import com.chuanzz.hackernewsapp.di.component.DaggerActivityComponent
import com.chuanzz.hackernewsapp.di.module.ActivityModule
import com.google.android.material.snackbar.Snackbar

abstract class BaseActivity : AppCompatActivity(), MvpView {

    private var toolbar: Toolbar? = null
    private lateinit var alertDialog: AlertDialog
    private var mActivityComponent: ActivityComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidNetworking.initialize(applicationContext)
        mActivityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .build()
        setContentView(R.layout.activity_main)
    }

    private fun initializeBasicComponent() {
        toolbar = findViewById(R.id.toolbar)
    }

    open fun getActivityComponent(): ActivityComponent? {
        return mActivityComponent
    }


    protected fun initializeBasic(layout: Int, title: String? = "") {
        setContentView(layout)
        initializeBasicComponent()
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (title == "") {
            supportActionBar?.setDisplayShowTitleEnabled(false)
        } else {
            supportActionBar?.setDisplayShowTitleEnabled(true)
        }
        supportActionBar?.title = title
        toolbar?.setNavigationOnClickListener { onBackPressed() }

        val fade = Fade()
        val decor = window.decorView.findViewById<View>(R.id.action_bar_container)

        fade.excludeTarget(decor, true)
        fade.excludeTarget(android.R.id.statusBarBackground, true)
        fade.excludeTarget(android.R.id.navigationBarBackground, true)

        window.enterTransition = fade
        window.exitTransition = fade

        alertDialog = AppUtils.createLoadingDialog(this)
    }

    protected fun removeNavigation() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    override fun showLoading() {
        hideLoading()
        alertDialog.show()
    }

    override fun hideLoading() {
        if (alertDialog.isShowing) {
            alertDialog.cancel()
            alertDialog.dismiss()
        }
    }

    open fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(
            findViewById(android.R.id.content),
            message, Snackbar.LENGTH_LONG
        )
        val sbView = snackbar.view
        val textView = sbView
            .findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this, R.color.white))
        snackbar.show()
    }

    override fun onError(message: String) {
        showSnackBar(message)
    }

    protected abstract fun setUp()
}