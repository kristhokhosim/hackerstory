package com.chuanzz.hackernewsapp.base

import com.chuanzz.hackernewsapp.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

open class BasePresenter<V : MvpView, I : MvpInteractor>(
    mvpInteractor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable?
) : MvpPresenter<V, I> {
    private val TAG = "BasePresenter"

    private var mSchedulerProvider: SchedulerProvider = schedulerProvider
    private var mCompositeDisposable: CompositeDisposable? = compositeDisposable

    private var mMvpView: V? = null
    private var mMvpInteractor: I? = mvpInteractor

    override fun onAttach(mvpView: V?) {
        mMvpView = mvpView
    }

    override fun onDetach() {
        mCompositeDisposable!!.dispose()
        mMvpView = null
        mMvpInteractor = null
    }

    override fun getMvpView(): V? {
        return mMvpView
    }

    override fun getInteractor(): I? {
        return mMvpInteractor
    }

    override fun isViewAttached(): Boolean {
        return mMvpView != null
    }

    fun getSchedulerProvider(): SchedulerProvider {
        return mSchedulerProvider
    }

    fun getCompositeDisposable(): CompositeDisposable? {
        return mCompositeDisposable
    }

}