package com.chuanzz.hackernewsapp.base

import com.chuanzz.hackernewsapp.api.ApiHelper

interface MvpInteractor {
    fun getApiHelper(): ApiHelper?
}