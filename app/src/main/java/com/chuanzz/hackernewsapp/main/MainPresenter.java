package com.chuanzz.hackernewsapp.main;

import android.annotation.SuppressLint;

import com.chuanzz.hackernewsapp.base.BasePresenter;
import com.chuanzz.hackernewsapp.base.MvpInteractor;
import com.chuanzz.hackernewsapp.base.MvpView;
import com.chuanzz.hackernewsapp.rx.SchedulerProvider;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;

public class MainPresenter<V extends MainMvpView & MvpView, I extends MainMvpInteractor & MvpInteractor>
        extends BasePresenter<V, I> implements MainMvpPresenter<V, I> {

    @Inject
    public MainPresenter(I mvpInteractor, SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewStories() {
        getCompositeDisposable().add(getInteractor().getStoriesApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .flatMap((Function<JSONArray, Observable<Integer>>) jsonArray -> {
                    List<Integer> list = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        list.add((Integer) jsonArray.get(i));
                    }
                    return Observable.fromIterable(list);
                })
                .toList()
                .subscribe(result -> getStories(result))
        );
    }

    @SuppressLint("CheckResult")
    private void getStories(List<Integer> ids) {
        if (!ids.isEmpty()) {
            Observable.fromArray(ids)
                    .flatMap((Function<List<Integer>, Observable<Integer>>) integers -> Observable.fromIterable(integers))
                    .subscribe(value -> getCompositeDisposable().add(getInteractor().getSpecificStoriesApiCall(value)
                            .subscribeOn(getSchedulerProvider().io())
                            .observeOn(getSchedulerProvider().ui())
                            .toList()
                            .subscribe(result -> getMvpView().onRetriveData(result), error -> {
                                getMvpView().onError(error.getMessage());
                                getMvpView().hideLoading();
                            })
                    ));
        }
    }
}
