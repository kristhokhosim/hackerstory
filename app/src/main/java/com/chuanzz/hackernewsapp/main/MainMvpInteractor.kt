package com.chuanzz.hackernewsapp.main

import com.chuanzz.hackernewsapp.base.MvpInteractor
import com.chuanzz.hackernewsapp.model.Story
import io.reactivex.Observable
import org.json.JSONArray

interface MainMvpInteractor : MvpInteractor {

    fun getStoriesApiCall(): Observable<JSONArray>?

    fun getSpecificStoriesApiCall(id: Int): Observable<Story>
}