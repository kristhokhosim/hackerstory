package com.chuanzz.hackernewsapp.main;

import com.chuanzz.hackernewsapp.api.ApiHelper;
import com.chuanzz.hackernewsapp.base.BaseInteractor;
import com.chuanzz.hackernewsapp.model.Story;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;

import javax.inject.Inject;

import io.reactivex.Observable;

public class MainInteractor extends BaseInteractor implements MainMvpInteractor {

    @Inject
    MainInteractor(ApiHelper apiHelper) {
        super(apiHelper);
    }

    @Nullable
    @Override
    public Observable<JSONArray> getStoriesApiCall() {
        return getApiHelper().getIdStories();
    }

    @NotNull
    @Override
    public Observable<Story> getSpecificStoriesApiCall(int id) {
        return getApiHelper().getSpecificDataStory(id);
    }
}
