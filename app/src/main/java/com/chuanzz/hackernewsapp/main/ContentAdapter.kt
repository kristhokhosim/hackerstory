package com.chuanzz.hackernewsapp.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chuanzz.hackernewsapp.R
import com.chuanzz.hackernewsapp.model.Story
import kotlinx.android.synthetic.main.item_content.view.*

class ContentAdapter(
    var stories: MutableList<Story> = arrayListOf(),
    var onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ContentAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_content, parent, false)
        return ItemViewHolder(v)
    }

    override fun getItemCount(): Int {
        return stories.size
    }

    override fun onBindViewHolder(itemHolder: ItemViewHolder, position: Int) {
        val story = stories[position]
        itemHolder.bind(story, onItemClickListener)
    }

    fun addAll(stories: MutableList<Story>) {
        this.stories.addAll(stories)
        notifyDataSetChanged()
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(story: Story, onItemClickListener: OnItemClickListener) {

            itemView.text_title.text = story.title
            itemView.text_comment.text = story.kids?.size.toString()
            itemView.text_score.text = story.score.toString()

            itemView.setOnClickListener {
                onItemClickListener.onClickStory(story)
            }
        }
    }

    interface OnItemClickListener {
        fun onClickStory(story: Story)
    }
}