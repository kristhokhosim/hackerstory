package com.chuanzz.hackernewsapp.main;

import com.chuanzz.hackernewsapp.base.MvpInteractor;
import com.chuanzz.hackernewsapp.base.MvpPresenter;

public interface MainMvpPresenter<V extends MainMvpView, I extends MainMvpInteractor & MvpInteractor> extends MvpPresenter<V, I> {
    void onViewStories();
}