package com.chuanzz.hackernewsapp.main

import com.chuanzz.hackernewsapp.base.MvpView
import com.chuanzz.hackernewsapp.model.Story

interface MainMvpView : MvpView {
    fun onRetriveData(story: List<Story>)
}