package com.chuanzz.hackernewsapp.main

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.chuanzz.hackernewsapp.R
import com.chuanzz.hackernewsapp.base.BaseActivity
import com.chuanzz.hackernewsapp.detailstory.DetailStoryActivity
import com.chuanzz.hackernewsapp.model.Story
import com.chuanzz.hackernewsapp.singleton.SingletonUtils
import com.chuanzz.hackernewsapp.utils.AppConstants
import com.chuanzz.hackernewsapp.utils.DividerItemDecoration
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainMvpView, ContentAdapter.OnItemClickListener {

    @Inject
    lateinit var mPresenter: MainMvpPresenter<MainMvpView, MainMvpInteractor>

    private lateinit var contentAdapter: ContentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeBasic(R.layout.activity_main, "Home")

        getActivityComponent()?.inject(this)

        mPresenter.onAttach(this)

        contentAdapter = ContentAdapter(onItemClickListener = this)

        rv_content.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    context, R.drawable.horizontal_divider_12,
                    showFirstDivider = true,
                    showLastDivider = true
                )
            )
            adapter = contentAdapter
        }

        setUp()
    }

    override fun setUp() {
        showLoading()
        mPresenter.onViewStories()
    }

    override fun onRetriveData(stories: List<Story>) {
        contentAdapter.addAll(stories.toMutableList())
        hideLoading()
    }

    override fun onClickStory(story: Story) {
        val intent = Intent(this, DetailStoryActivity::class.java)
        intent.putExtra(AppConstants.EXTRA_DATA, story)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        text_title_favorite.text = SingletonUtils.FavoriteSingleton.favorite?.title ?: ""
    }
}
