package com.chuanzz.hackernewsapp.detailstory

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.chuanzz.hackernewsapp.R
import com.chuanzz.hackernewsapp.base.BaseActivity
import com.chuanzz.hackernewsapp.main.DetailStoryMvpView
import com.chuanzz.hackernewsapp.model.Comment
import com.chuanzz.hackernewsapp.model.Story
import com.chuanzz.hackernewsapp.singleton.SingletonUtils
import com.chuanzz.hackernewsapp.utils.AppConstants
import com.chuanzz.hackernewsapp.utils.DividerItemDecoration
import kotlinx.android.synthetic.main.activity_detail_story.*
import javax.inject.Inject

class DetailStoryActivity : BaseActivity(), DetailStoryMvpView {

    @Inject
    lateinit var mPresenter: DetailStoryMvpPresenter<DetailStoryMvpView, DetailStoryMvpInteractor>

    private var story: Story? = null

    private lateinit var commentAdapter: CommentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        story = intent.extras?.getParcelable(AppConstants.EXTRA_DATA)

        initializeBasic(R.layout.activity_detail_story, story?.by)

        getActivityComponent()?.inject(this)

        mPresenter.onAttach(this)

        commentAdapter = CommentAdapter()

        rv_comment.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    context, R.drawable.horizontal_divider_12,
                    showFirstDivider = true,
                    showLastDivider = true
                )
            )
            adapter = commentAdapter
        }

        image_favorite.setOnClickListener {
            image_favorite.setImageResource(R.drawable.ic_star_filled)
            SingletonUtils.FavoriteSingleton.favorite = story
        }

        setUp()
    }

    override fun setUp() {
        showLoading()
        mPresenter.onViewComment(story?.kids)
        text_title.text = story?.title
        text_by.text = story?.by
        text_content_description.text = story?.title
    }

    override fun getComments(comments: List<Comment>) {
        commentAdapter.addAll(comments.toMutableList())
        hideLoading()
    }
}
