package com.chuanzz.hackernewsapp.detailstory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.chuanzz.hackernewsapp.R
import com.chuanzz.hackernewsapp.model.Comment
import kotlinx.android.synthetic.main.item_content.view.*

class CommentAdapter(
    var comments: MutableList<Comment> = arrayListOf()
) : RecyclerView.Adapter<CommentAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return ItemViewHolder(v)
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    override fun onBindViewHolder(itemHolder: ItemViewHolder, position: Int) {
        val comment = comments[position]
        itemHolder.bind(comment)
    }

    fun add(comment: Comment) {
        comments.add(comment)
        notifyItemInserted(comments.size - 1)
    }

    fun addAll(comments: MutableList<Comment>) {
        this.comments.addAll(comments)
        notifyDataSetChanged()
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(comment: Comment) {
            if (comment.komentar?.trim()?.length!! > 0)
                itemView.text_comment.text =
                    HtmlCompat.fromHtml(comment.komentar ?: "", HtmlCompat.FROM_HTML_MODE_LEGACY);
        }
    }
}