package com.chuanzz.hackernewsapp.detailstory;

import com.chuanzz.hackernewsapp.api.ApiHelper;
import com.chuanzz.hackernewsapp.base.BaseInteractor;
import com.chuanzz.hackernewsapp.model.Comment;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import io.reactivex.Observable;

public class DetailStoryInteractor extends BaseInteractor implements DetailStoryMvpInteractor {

    @Inject
    DetailStoryInteractor(ApiHelper apiHelper) {
        super(apiHelper);
    }

    @NotNull
    @Override
    public Observable<Comment> getCommentApiCall(int id) {
        return getApiHelper().getDataComment(id);
    }
}
