package com.chuanzz.hackernewsapp.detailstory

import com.chuanzz.hackernewsapp.base.MvpInteractor
import com.chuanzz.hackernewsapp.model.Comment
import io.reactivex.Observable

interface DetailStoryMvpInteractor : MvpInteractor {
    fun getCommentApiCall(id: Int): Observable<Comment>
}