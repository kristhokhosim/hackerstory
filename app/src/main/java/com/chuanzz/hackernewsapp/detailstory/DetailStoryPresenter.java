package com.chuanzz.hackernewsapp.detailstory;

import android.annotation.SuppressLint;

import com.chuanzz.hackernewsapp.base.BasePresenter;
import com.chuanzz.hackernewsapp.base.MvpInteractor;
import com.chuanzz.hackernewsapp.base.MvpView;
import com.chuanzz.hackernewsapp.main.DetailStoryMvpView;
import com.chuanzz.hackernewsapp.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;

public class DetailStoryPresenter<V extends DetailStoryMvpView & MvpView, I extends DetailStoryMvpInteractor & MvpInteractor>
        extends BasePresenter<V, I> implements DetailStoryMvpPresenter<V, I> {

    @Inject
    public DetailStoryPresenter(I mvpInteractor, SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onViewComment(List<Integer> ids) {
        if (ids != null) {
            if (!ids.isEmpty()) {
                Observable.fromArray(ids)
                        .flatMap((Function<List<Integer>, Observable<Integer>>) integers -> Observable.fromIterable(integers))
                        .subscribe(value -> getCompositeDisposable().add(getInteractor().getCommentApiCall(value)
                                .subscribeOn(getSchedulerProvider().io())
                                .observeOn(getSchedulerProvider().ui())
                                .toList()
                                .subscribe(result -> getMvpView().getComments(result), error -> {
                                    getMvpView().onError(error.getMessage());
                                    getMvpView().hideLoading();
                                })
                        ));
            }
        }
    }
}
