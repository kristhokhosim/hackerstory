package com.chuanzz.hackernewsapp.detailstory;

import com.chuanzz.hackernewsapp.base.MvpInteractor;
import com.chuanzz.hackernewsapp.base.MvpPresenter;
import com.chuanzz.hackernewsapp.main.DetailStoryMvpView;

import java.util.List;

public interface DetailStoryMvpPresenter<V extends DetailStoryMvpView, I extends DetailStoryMvpInteractor & MvpInteractor> extends MvpPresenter<V, I> {
    void onViewComment(List<Integer> ids);
}