package com.chuanzz.hackernewsapp.main

import com.chuanzz.hackernewsapp.base.MvpView
import com.chuanzz.hackernewsapp.model.Comment
import com.chuanzz.hackernewsapp.model.Story

interface DetailStoryMvpView : MvpView {
    fun getComments(comments : List<Comment>)
}