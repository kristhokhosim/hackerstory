package com.chuanzz.hackernewsapp

import android.content.Context
import androidx.appcompat.app.AlertDialog


class AppUtils {
    companion object {
        fun createLoadingDialog(context: Context): AlertDialog {
            val alertDialog = AlertDialog.Builder(context)
            alertDialog.setCancelable(false)
            alertDialog.setView(R.layout.progress_dialog)

            return alertDialog.create()
        }
    }
}