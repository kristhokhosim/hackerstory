package com.chuanzz.hackernewsapp.api

import com.chuanzz.hackernewsapp.model.Comment
import com.chuanzz.hackernewsapp.model.Story
import io.reactivex.Observable
import org.json.JSONArray

interface ApiHelper {
    fun getIdStories(): Observable<JSONArray>

    fun getSpecificDataStory(id: Int): Observable<Story>

    fun getDataComment(id: Int): Observable<Comment>
}