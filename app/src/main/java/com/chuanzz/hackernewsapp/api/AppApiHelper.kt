package com.chuanzz.hackernewsapp.api

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.chuanzz.hackernewsapp.model.Comment
import com.chuanzz.hackernewsapp.model.Story
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observable
import org.json.JSONArray
import javax.inject.Inject

class AppApiHelper @Inject constructor(context: AppCompatActivity) : ApiHelper {

    private var mContext: Context = context

    override fun getIdStories(): Observable<JSONArray> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_TOP_STORIES)
            .build()
            .jsonArrayObservable
    }

    override fun getSpecificDataStory(id: Int): Observable<Story> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_PATH_GET_DATA)
            .addPathParameter("id", id.toString())
            .build()
            .getObjectObservable(Story::class.java)
    }

    override fun getDataComment(id: Int): Observable<Comment> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_PATH_GET_DATA)
            .addPathParameter("id", id.toString())
            .build()
            .getObjectObservable(Comment::class.java)
    }
}