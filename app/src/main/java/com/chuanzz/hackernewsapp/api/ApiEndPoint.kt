package com.chuanzz.hackernewsapp.api

class ApiEndPoint {
    companion object {
        const val BASE_URL = "https://hacker-news.firebaseio.com/v0/"
        const val ENDPOINT_TOP_STORIES = "$BASE_URL/topstories.json"
        const val ENDPOINT_PATH_GET_DATA = "$BASE_URL/item/{id}.json"
    }
}