package com.chuanzz.hackernewsapp.singleton

import com.chuanzz.hackernewsapp.model.Story

class SingletonUtils {
    object FavoriteSingleton {
        var favorite: Story? = null
    }
}