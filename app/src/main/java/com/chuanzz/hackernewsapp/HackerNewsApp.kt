package com.chuanzz.hackernewsapp

import android.app.Application
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.gsonparserfactory.GsonParserFactory
import com.chuanzz.hackernewsapp.di.component.ApplicationComponent
import com.chuanzz.hackernewsapp.di.component.DaggerApplicationComponent
import com.chuanzz.hackernewsapp.di.module.ApplicationModule
import com.chuanzz.hackernewsapp.utils.BooleanTypeAdapter
import com.google.gson.GsonBuilder

class HackerNewsApp : Application() {

    private var mApplicationComponent: ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()

        mApplicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule()).build()

        AndroidNetworking.initialize(applicationContext)
        AndroidNetworking.setParserFactory(
            GsonParserFactory(
                GsonBuilder()
                    .registerTypeAdapter(Boolean::class.javaPrimitiveType, BooleanTypeAdapter())
                    .create()
            )
        )
    }

    fun getComponent(): ApplicationComponent? {
        return mApplicationComponent
    }

}