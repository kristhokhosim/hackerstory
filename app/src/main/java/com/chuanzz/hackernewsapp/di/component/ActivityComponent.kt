package com.chuanzz.hackernewsapp.di.component

import com.chuanzz.hackernewsapp.detailstory.DetailStoryActivity
import com.chuanzz.hackernewsapp.di.module.ActivityModule
import com.chuanzz.hackernewsapp.main.MainActivity
import dagger.Component

@Component(modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(detailStoryActivity: DetailStoryActivity)
}