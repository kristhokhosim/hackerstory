package com.chuanzz.hackernewsapp.di.component

import com.chuanzz.hackernewsapp.HackerNewsApp
import com.chuanzz.hackernewsapp.detailstory.DetailStoryActivity
import com.chuanzz.hackernewsapp.di.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun inject(app: HackerNewsApp)
}