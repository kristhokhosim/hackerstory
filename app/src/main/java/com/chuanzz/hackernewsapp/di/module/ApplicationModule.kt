package com.chuanzz.hackernewsapp.di.module

import com.chuanzz.hackernewsapp.api.ApiHelper
import com.chuanzz.hackernewsapp.api.AppApiHelper
import com.chuanzz.hackernewsapp.utils.BooleanTypeAdapter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    fun provideApiHelper(appApiHelper: AppApiHelper?): ApiHelper? {
        return appApiHelper
    }

    @Provides
    fun provideGson(): Gson? {
        return GsonBuilder()
            .registerTypeAdapter(Boolean::class.javaPrimitiveType, BooleanTypeAdapter())
            .create()
    }
}