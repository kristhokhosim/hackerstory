package com.chuanzz.hackernewsapp.di.module

import androidx.appcompat.app.AppCompatActivity
import com.chuanzz.hackernewsapp.api.ApiHelper
import com.chuanzz.hackernewsapp.api.AppApiHelper
import com.chuanzz.hackernewsapp.detailstory.DetailStoryInteractor
import com.chuanzz.hackernewsapp.detailstory.DetailStoryMvpInteractor
import com.chuanzz.hackernewsapp.detailstory.DetailStoryMvpPresenter
import com.chuanzz.hackernewsapp.detailstory.DetailStoryPresenter
import com.chuanzz.hackernewsapp.main.*
import com.chuanzz.hackernewsapp.rx.AppSchedulerProvider
import com.chuanzz.hackernewsapp.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class ActivityModule(appCompatActivity: AppCompatActivity) {

    private var mActivity: AppCompatActivity = appCompatActivity

    @Provides
    fun provideActivity(): AppCompatActivity {
        return mActivity
    }

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

    @Provides
    fun provideMainPresenter(presenter: MainPresenter<MainMvpView, MainMvpInteractor>): MainMvpPresenter<MainMvpView, MainMvpInteractor> =
        presenter

    @Provides
    fun provideMainMvpInteractor(interactor: MainInteractor): MainMvpInteractor = interactor

    @Provides
    fun provideDetailStoryPresenter(presenter: DetailStoryPresenter<DetailStoryMvpView, DetailStoryMvpInteractor>): DetailStoryMvpPresenter<DetailStoryMvpView, DetailStoryMvpInteractor> =
        presenter

    @Provides
    fun provideDetailStoryMvpInteractor(interactor: DetailStoryInteractor): DetailStoryMvpInteractor =
        interactor

    @Provides
    fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper
}